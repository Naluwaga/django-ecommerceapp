# Django Ecommerce Application

Welcome to our Django Ecommerce Application! This application is designed to provide a platform for businesses to sell their products online and for customers to browse, purchase, and review products. Below, you'll find information on how to set up and use the application.

## Features

- **User Registration and Authentication:** Users can create accounts and log in to access features such as browsing products, adding items to cart, and making purchases.

- **Product Management:** Business owners can add, edit, and delete products, including uploading product images and videos, setting prices, and managing inventory.

- **Shopping Cart:** Customers can add products to their shopping cart, view and modify cart contents, and proceed to checkout to complete their purchases.

- **Order Management:** Users can view their order history, track order status, and leave reviews for products they have purchased.

## Installation

1. Clone the repository:

   ```
   git clone https://gitlab.com/django-ecommerceapp/django-ecommerceapp.git
   ```

2. Install dependencies:

   ```
   cd django-ecommerce
   pip install -r requirements.txt
   ```

3. Set up the database:

   - Configure your database settings in `settings.py`.
   - Run migrations to create database tables:
     ```
     python manage.py makemigrations
     python manage.py migrate
     ```

4. Create a superuser:

   ```
   python manage.py createsuperuser
   ```

5. Start the development server:

   ```
   python manage.py runserver
   ```

6. Access the admin panel:

   - Log in to the admin panel using the superuser credentials you created.
   - Add products and manage other aspects of the application.

## Usage

- **User Registration and Authentication:**
  - Navigate to the registration page and create a new account.
  - Log in with your credentials to access the full functionality of the application.

- **Product Management:**
  - Log in to the admin panel and navigate to the product management section.
  - Add new products, edit existing ones, and manage inventory and pricing.

- **Shopping Cart:**
  - Browse products and click on "Add to Cart" to add items to your shopping cart.
  - View and modify your cart contents, including updating quantities and removing items.
  - Proceed to checkout to complete your purchase.

- **Order Management:**
  - After completing a purchase, view your order history and track order status.
  - Leave reviews for products you have purchased to share your feedback with other users.

## Contributing

We welcome contributions from the community! If you'd like to contribute to the development of this project, please follow these steps:

1. Fork the repository.
2. Create a new branch 
3. Make your changes and commit them 
4. Push to the branch
5. Create a new pull request.

## License

